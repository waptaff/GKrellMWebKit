GKrellMWebKit is a GKrellM plugin that allows displaying WebKit windows inside
GKrellM.

# Prerequisites

To compile GKrellMWebKit some developement packages are needed.  Here are some
examples to get started:

## CentOS 6

```shell
# GkrellM is found in the EPEL repository.
sudo yum install epel-release
# Update package list to include EPEL packages.
sudo yum update
# Install prerequisites.
sudo yum install gettext gkrellm-devel libtool webkitgtk-devel
```

## Debian 9 (Stretch)

```shell
sudo apt-get install autopoint gkrellm libwebkitgtk-dev
```

# Compilation / Installation

GKrellMWebKit uses GNU autotools, so installation should be straightforward:

```shell
autoreconf --install
./configure
make
make install
```

