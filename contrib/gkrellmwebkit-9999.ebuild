# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit autotools gkrellm-plugin l10n vcs-snapshot

PLOCALES="fr"

DESCRIPTION="A GKrellM plugin which embeds WebKit windows"
HOMEPAGE="https://gitlab.com/waptaff/GKrellMWebKit/"
[[ ${PV} == 9999 ]] \
	&& SRC_URI="https://gitlab.com/waptaff/GKrellMWebKit/repository/archive.tar.bz2?ref=master -> ${P}.tar.bz2" \
	|| SRC_URI="https://gitlab.com/waptaff/GKrellMWebKit/repository/archive.tar.bz2?ref=${PV} -> ${P}.tar.bz2"

LICENSE="GPL3+"
SLOT="0"
KEYWORDS="~x86"
IUSE="nls"

PLUGIN_SO="src/.libs/gkrellmwebkit.so"

DEPEND="
	app-admin/gkrellm[X]
	dev-libs/glib:2
	net-libs/webkit-gtk:2
	x11-libs/gtk+:2
"

RDEPEND="${DEPEND}
	nls? ( sys-devel/gettext )
"

src_prepare() {
	eapply_user
	eautoreconf
}

src_configure() {
	econf \
		$(use_enable nls)
}

src_install() {
	default
	dodoc AUTHORS README
}

