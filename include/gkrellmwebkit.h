/* GKrellMWebKit - Allows mini-browser windows inside GKrellM
 * Copyright (C) 2017 Patrice Levesque
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GKRELLMWEBKIT_H__
#define __GKRELLMWEBKIT_H__

#define GWK_PLUGIN_PLACEMENT   MON_CPU
#define GWK_PLUGIN_NAME        "GKrellMWebKit"
#define GWK_PLUGIN_VERSION     "0.1"
#define GWK_PLUGIN_KEYWORD     "webkit"
#define GWK_MAX_FRAMES         16
#define GWK_MIN_FRAME_HEIGHT   8
#define GWK_STD_FRAME_HEIGHT   64
#define GWK_MAX_FRAME_HEIGHT   2048
#define GWK_DEFAULT_BROWSER    "xdg-open"

typedef struct {
	WebKitWebView             *web_view;
	WebKitViewportAttributes  *attributes;
	gchar                     *uri;
	gchar                     *popup_uri;
	gint                      height;
	gint                      period;
} gwk_frame;

static void gwk_create_plugin      (GtkWidget *vbox, gint first_create);
static void gwk_create_tab         (GtkWidget *tab_vbox);
static void gwk_load_plugin_config (gchar *arg);
static void gwk_save_plugin_config (FILE *f);
static void gwk_update_plugin      ();

static WebKitWebView* gwk_create_web_view_cb(
	WebKitWebView  *web_view,
	WebKitWebFrame *frame,
	gpointer        user_data
);

static gboolean gwk_new_window_policy_decision_requested_cb(
	WebKitWebView             *web_view,
	WebKitWebFrame            *frame,
	WebKitNetworkRequest      *request,
	WebKitWebNavigationAction *navigation_action,
	WebKitWebPolicyDecision   *policy_decision,
	gpointer                   user_data
);

static void gwk_cfg_browser_cb(
	GtkWidget *widget,
	gpointer data
);

static void gwk_cfg_height_cb(
	GtkWidget *widget,
	gpointer data
);

static void gwk_cfg_period_cb(
	GtkWidget *widget,
	gpointer data
);

static void gwk_cfg_uri_cb(
	GtkWidget *widget,
	gpointer data
);

static void gwk_cfg_uri_event_cb(
	GtkWidget *widget,
	GdkEvent *event,
	gpointer data
);

static void gwk_cfg_num_used_frames_cb(
	GtkWidget *widget,
	gpointer data
);

#endif /* __GKRELLMWEBKIT_H__ */

