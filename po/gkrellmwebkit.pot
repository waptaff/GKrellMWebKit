# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Patrice Levsque
# This file is distributed under the same license as the gkrellmwebkit package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gkrellmwebkit 0.1\n"
"Report-Msgid-Bugs-To: gkrellmwebkit.wayne@ptaff.ca\n"
"POT-Creation-Date: 2017-06-27 15:26-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/gkrellmwebkit.c:39
msgid "About"
msgstr ""

#: src/gkrellmwebkit.c:41
#, c-format
msgid ""
"%1$s %2$s\n"
"\n"
"%3$s\n"
"\n"
"Copyright (c) 2017 Patrice Levesque\n"
"gkrellmwebkit.wayne@ptaff.ca\n"
"%1$s comes with ABSOLUTELY NO WARRANTY;\n"
"see the file COPYING for details.\n"
msgstr ""

#: src/gkrellmwebkit.c:48 src/gkrellmwebkit.c:74
msgid "Height (in pixels)"
msgstr ""

#: src/gkrellmwebkit.c:50 src/gkrellmwebkit.c:77
msgid "Number of seconds between reloads (0 to disable)"
msgstr ""

#: src/gkrellmwebkit.c:52
#, c-format
msgid "Frame #%d"
msgstr ""

#: src/gkrellmwebkit.c:53 src/gkrellmwebkit.c:69
msgid "URI"
msgstr ""

#: src/gkrellmwebkit.c:54
msgid "Info"
msgstr ""

#: src/gkrellmwebkit.c:57
msgid "This plugin allows mini-browser windows inside GKrellM, that can be used to display (typically custom) web pages."
msgstr ""

#: src/gkrellmwebkit.c:60 src/gkrellmwebkit.c:106
msgid "Setup"
msgstr ""

#: src/gkrellmwebkit.c:61 src/gkrellmwebkit.c:103
msgid "Browser path for window pop-ups"
msgstr ""

#: src/gkrellmwebkit.c:62
msgid "Application used when a browser frame requests a pop-up window; the URL will be the first and only parameter.  xdg-open is a sane default."
msgstr ""

#: src/gkrellmwebkit.c:65 src/gkrellmwebkit.c:105
msgid "Number of frames"
msgstr ""

#: src/gkrellmwebkit.c:66
msgid "Number of browser frames to display; to get zero browser frame, plugin must be disabled."
msgstr ""

#: src/gkrellmwebkit.c:68
msgid "Frames"
msgstr ""

#: src/gkrellmwebkit.c:70
msgid "Initial URI to load into the browser frame.  The file:// prefix must be used to use a local file path.  URI can point to a webpage, or anything a browser can handle, e.g. an image, a video, and so on."
msgstr ""

#: src/gkrellmwebkit.c:75
msgid "Height of the browser frame, in pixels.  Size is fixed by this setting, the browser cannot resize itself."
msgstr ""

#: src/gkrellmwebkit.c:78
msgid "When set to a non-zero value, sends a reload request to the browser window at the requested frequency."
msgstr ""

#: src/gkrellmwebkit.c:80
msgid "Webpage creation"
msgstr ""

#: src/gkrellmwebkit.c:81
msgid "Most likely a custom web page will need to be written by the user to fill the frames, as most webpages, even mobile ones, display poorly at the typical width of GKrellM windows.  Here are some recommendations."
msgstr ""

#: src/gkrellmwebkit.c:85
msgid "* A liquid layout works better in the long run than a fixed one; changes in GKrellM width or theme can ruin a fixed layout."
msgstr ""

#: src/gkrellmwebkit.c:88
msgid "* Vector images (SVG) sized using percentages work better than fixed-size pixel images (PNG/JPG), as they scale cleanly with width changes."
msgstr ""

#: src/gkrellmwebkit.c:91
msgid "* A <body> transparent background allows easy integration with the GKrellM theme."
msgstr ""

#: src/gkrellmwebkit.c:93
msgid "* The \"title\" attribute on HTML elements allows easy addition of tooltips."
msgstr ""

#: src/gkrellmwebkit.c:95
msgid "* Pop-up windows can be spawned by setting the \"target\" attribute on \"a\" tags to \"_blank\"."
msgstr ""

#: src/gkrellmwebkit.c:100
msgid "Allows mini-browser windows inside GKrellM."
msgstr ""
