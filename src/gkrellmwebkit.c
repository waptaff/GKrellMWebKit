/* GKrellMWebKit - Allows mini-browser windows inside GKrellM
 * Copyright (C) 2017 Patrice Levesque
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <webkit/webkit.h>
#include <gkrellm2/gkrellm.h>
#include "gkrellmwebkit.h"
#include "gettext.h"

/* Global variables {{{ */
static GkrellmMonitor *gwk_monitor         = NULL;
static GkrellmTicks   *gwk_ticks           = NULL;
static GtkWidget      *gwk_gk_vbox         = NULL;
static GtkWidget      *gwk_tabs            = NULL;
static int             gwk_height          = 0;
/* }}} */

/* Current plugin configuration {{{ */
static int             gwk_cfg_used_frames = 0;
static gchar          *gwk_cfg_browser     = NULL;
static gwk_frame      *gwk_cfg_frames      = NULL;
/* }}} */

/* Localizable strings {{{ */
static gchar *gwk_str_about_tab_title      = N_("About");
static gchar *gwk_str_about_text           = N_(
	"%1$s %2$s\n\n"
	"%3$s\n\n"
	"Copyright (c) 2017 Patrice Levesque\n"
	"gkrellmwebkit.wayne@ptaff.ca\n"
	"%1$s comes with ABSOLUTELY NO WARRANTY;\n"
	"see the file COPYING for details.\n"
);
static gchar *gwk_str_frame_height_title   = N_("Height (in pixels)");
static gchar *gwk_str_frame_period_title   = N_(
	"Number of seconds between reloads (0 to disable)"
);
static gchar *gwk_str_frame_tab_title      = N_("Frame #%d");
static gchar *gwk_str_frame_uri_title      = N_("URI");
static gchar *gwk_str_info_tab_title       = N_("Info");
static gchar *gwk_str_info_text[]          = {
	"<h>", GWK_PLUGIN_NAME, "\n\n",
	N_("This plugin allows mini-browser windows inside GKrellM," \
		" that can be used to display (typically custom)" \
		" web pages."), "\n\n",
	"<h>", N_("Setup"), "\n\n",
	"<b>", N_("Browser path for window pop-ups"), "\n",
	N_("Application used when a browser frame requests a" \
		" pop-up window; the URL will be the first and only" \
		" parameter.  xdg-open is a sane default."), "\n\n",
	"<b>", N_("Number of frames"), "\n",
	N_("Number of browser frames to display; to get zero browser" \
		" frame, plugin must be disabled."), "\n\n",
	"<h>", N_("Frames"), "\n\n",
	"<b>", N_("URI"), "\n",
	N_("Initial URI to load into the browser frame.  The file://" \
		" prefix must be used to use a local file path.  URI can" \
		" point to a webpage, or anything a browser can handle, e.g." \
		" an image, a video, and so on."), "\n\n",
	"<b>", N_("Height (in pixels)"), "\n",
	N_("Height of the browser frame, in pixels.  Size is fixed by" \
		" this setting, the browser cannot resize itself."), "\n\n",
	"<b>", N_("Number of seconds between reloads (0 to disable)"), "\n",
	N_("When set to a non-zero value, sends a reload request to the" \
		" browser window at the requested frequency."), "\n\n",
	"<h>", N_("Webpage creation"), "\n\n",
	N_("Most likely a custom web page will need to be written by the" \
		" user to fill the frames, as most webpages, even mobile" \
		" ones, display poorly at the typical width of GKrellM" \
		" windows.  Here are some recommendations."), "\n\n",
	N_("* A liquid layout works better in the long run than a fixed" \
		" one; changes in GKrellM width or theme can ruin a fixed" \
		" layout."), "\n\n",
	N_("* Vector images (SVG) sized using percentages work better" \
		" than fixed-size pixel images (PNG/JPG), as they scale" \
		" cleanly with width changes."), "\n\n",
	N_("* A <body> transparent background allows easy integration" \
		" with the GKrellM theme."), "\n\n",
	N_("* The \"title\" attribute on HTML elements allows easy" \
		" addition of tooltips."), "\n\n",
	N_("* Pop-up windows can be spawned by setting the \"target\"" \
		" attribute on \"a\" tags to \"_blank\"."), "\n\n"
};

static gchar *gwk_str_plugin_description   = N_(
	"Allows mini-browser windows inside GKrellM."
);
static gchar *gwk_str_setup_browser_title  = N_(
	"Browser path for window pop-ups"
);
static gchar *gwk_str_setup_num_of_frames  = N_("Number of frames");
static gchar *gwk_str_setup_tab_title      = N_("Setup");
/* }}} */

/* Helper functions {{{ */
static void gwk_create_tab_about() { /* {{{ */
	GtkWidget *about;
	GtkWidget *vbox;
	vbox = gkrellm_gtk_framed_notebook_page(
		GTK_WIDGET(gwk_tabs),
		_(gwk_str_about_tab_title)
	);
	about = gtk_label_new(
		g_strdup_printf(
			_(gwk_str_about_text),
			GWK_PLUGIN_NAME,
			GWK_PLUGIN_VERSION,
			_(gwk_str_plugin_description)
		)
	);
	gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(about), TRUE, TRUE, 0);
} /* }}} */
static void gwk_create_tab_frame(int i) { /* {{{ */
	GtkSpinButton  *height_entry;
	GtkAdjustment  *height_entry_buffer;
	GtkSpinButton  *period_entry;
	GtkAdjustment  *period_entry_buffer;
	GtkWidget      *uri_entry;
	GtkEntryBuffer *uri_entry_buffer;
	GtkWidget      *vbox;
	GtkWidget      *vbox1;
	GtkWidget      *vbox2;
	GtkWidget      *vbox3;
	GtkWidget      *tmp_page;
	vbox = gkrellm_gtk_framed_notebook_page(
		GTK_WIDGET(gwk_tabs),
		g_strdup_printf(_(gwk_str_frame_tab_title), i + 1)
	);
	vbox1 = gkrellm_gtk_category_vbox(
		GTK_WIDGET(vbox),
		_(gwk_str_frame_uri_title),
		4,
		0,
		TRUE
	);
	uri_entry_buffer =
		gtk_entry_buffer_new(gwk_cfg_frames[i].uri, -1);
	uri_entry = gtk_entry_new_with_buffer(uri_entry_buffer);
	gtk_box_pack_start(
		GTK_BOX(vbox1),
		GTK_WIDGET(uri_entry),
		FALSE,
		FALSE,
		0
	);
	g_signal_connect(
		G_OBJECT(uri_entry),
		"focus-out-event",
		G_CALLBACK(gwk_cfg_uri_event_cb),
		GINT_TO_POINTER(i)
	);
	g_signal_connect(
		G_OBJECT(uri_entry),
		"activate",
		G_CALLBACK(gwk_cfg_uri_cb),
		GINT_TO_POINTER(i)
	);
	vbox2 = gkrellm_gtk_category_vbox(
		GTK_WIDGET(vbox),
		_(gwk_str_frame_height_title),
		4,
		0,
		TRUE
	);
	height_entry_buffer = GTK_ADJUSTMENT(gtk_adjustment_new(
		gwk_cfg_frames[i].height,
		GWK_MIN_FRAME_HEIGHT,
		GWK_MAX_FRAME_HEIGHT,
		1,
		1,
		0
	));
	height_entry = GTK_SPIN_BUTTON(gtk_spin_button_new(
		height_entry_buffer,
		1,
	0));
	gtk_box_pack_start(
		GTK_BOX(vbox2),
		GTK_WIDGET(height_entry),
		FALSE,
		FALSE,
		0
	);
	g_signal_connect(
		G_OBJECT(height_entry),
		"value-changed",
		G_CALLBACK(gwk_cfg_height_cb),
		GINT_TO_POINTER(i)
	);
	vbox3 = gkrellm_gtk_category_vbox(
		GTK_WIDGET(vbox),
		_(gwk_str_frame_period_title),
		4,
		0,
		TRUE
	);
	period_entry_buffer = GTK_ADJUSTMENT(gtk_adjustment_new(
		gwk_cfg_frames[i].period,
		0,
		G_MAXINT,
		1,
		1,
		0
	));
	period_entry = GTK_SPIN_BUTTON(gtk_spin_button_new(
		period_entry_buffer,
		1,
	0));
	gtk_box_pack_start(
		GTK_BOX(vbox3),
		GTK_WIDGET(period_entry),
		FALSE,
		FALSE,
		0
	);
	g_signal_connect(
		G_OBJECT(period_entry),
		"value-changed",
		G_CALLBACK(gwk_cfg_period_cb),
		GINT_TO_POINTER(i)
	);
	/* Move tab to after the setup tab */
	tmp_page = gtk_notebook_get_nth_page(GTK_NOTEBOOK(gwk_tabs), -1);
	gtk_notebook_reorder_child(
		GTK_NOTEBOOK(gwk_tabs),
		tmp_page,
		i + 1
	);
} /* }}} */
static void gwk_remove_tab_frame(int i) { /* {{{ */
	gtk_notebook_remove_page(
		GTK_NOTEBOOK(gwk_tabs),
		i + 1
	);
} /* }}} */
static void gwk_create_tab_info() { /* {{{ */
	GtkWidget *info;
	GtkWidget *vbox;
	unsigned int i;
	vbox = gkrellm_gtk_framed_notebook_page(
		GTK_WIDGET(gwk_tabs),
		_(gwk_str_info_tab_title)
	);
	info = gkrellm_gtk_scrolled_text_view(
		vbox,
		NULL,
		GTK_POLICY_NEVER,
		GTK_POLICY_AUTOMATIC
	);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(info), GTK_WRAP_WORD);
	for (i = 0; i < sizeof(gwk_str_info_text) / sizeof(gchar *); i++) {
		gkrellm_gtk_text_view_append(info, _(gwk_str_info_text[i]));
	};
} /* }}} */
static void gwk_create_tab_setup() { /* {{{ */
	GtkWidget     *vbox;
	GtkWidget     *vbox1;
	GtkWidget     *vbox2;
	GtkWidget     *browser_combo_box;
	GtkSpinButton *num_used_frames_entry;
	GtkAdjustment *num_used_frames_entry_buffer;
	vbox = gkrellm_gtk_framed_notebook_page(
		GTK_WIDGET(gwk_tabs),
		_(gwk_str_setup_tab_title)
	);
	vbox1 = gkrellm_gtk_category_vbox(
		GTK_WIDGET(vbox),
		_(gwk_str_setup_browser_title),
		4,
		0,
		TRUE
	);
	browser_combo_box = gtk_combo_box_text_new_with_entry();
	gtk_box_pack_start(
		GTK_BOX(vbox1),
		GTK_WIDGET(browser_combo_box),
		FALSE,
		FALSE,
		0
	);
	gtk_combo_box_text_append_text(
		GTK_COMBO_BOX_TEXT(browser_combo_box),
		gwk_cfg_browser
	);
	/* Add default value choice if not already chosen */
	if (strcmp(gwk_cfg_browser, GWK_DEFAULT_BROWSER)) {
		gtk_combo_box_text_append_text(
			GTK_COMBO_BOX_TEXT(browser_combo_box),
			GWK_DEFAULT_BROWSER
		);
	};
	gtk_combo_box_set_active(GTK_COMBO_BOX(browser_combo_box), 0);
	g_signal_connect(
		G_OBJECT(browser_combo_box),
		"changed",
		G_CALLBACK(gwk_cfg_browser_cb),
		NULL
	);
	vbox2 = gkrellm_gtk_category_vbox(
		GTK_WIDGET(vbox),
		_(gwk_str_setup_num_of_frames),
		4,
		0,
		TRUE
	);
	num_used_frames_entry_buffer = GTK_ADJUSTMENT(gtk_adjustment_new(
		gwk_cfg_used_frames,
		1,
		GWK_MAX_FRAMES,
		1,
		1,
		0
	));
	num_used_frames_entry = GTK_SPIN_BUTTON(gtk_spin_button_new(
		num_used_frames_entry_buffer,
		1,
	0));
	gtk_box_pack_start(
		GTK_BOX(vbox2),
		GTK_WIDGET(num_used_frames_entry),
		FALSE,
		FALSE,
		0
	);
	g_signal_connect(
		G_OBJECT(num_used_frames_entry),
		"value-changed",
		G_CALLBACK(gwk_cfg_num_used_frames_cb),
		NULL
	);
} /* }}} */
static void gwk_reallocate_monitor_height() { /* {{{ */
	gint i;
	gint total_height = 0;
	/* Calculate the height of all active monitors */
	for (i = 0; i < gwk_cfg_used_frames; i++) {
		total_height += gwk_cfg_frames[i].height;
	};
	/* Remove the previously saved preallocated height */
	gkrellm_freeze_side_frame_packing();
	gkrellm_monitor_height_adjust(-gwk_height);
	/* Save preallocated height and readjust */
	gwk_height = total_height;
	gkrellm_monitor_height_adjust(gwk_height);
	gkrellm_thaw_side_frame_packing();
} /* }}} */
static void gwk_apply_webkit_bg(int i) { /* {{{ */
	/*
	 * The principle here: fetch the GKrellM background image used for
	 * panels, scale it to the WebKit frame size, make a PNG out of it,
	 * and inject that PNG using CSS into the frame.  All using
	 * base64-encoded data URIs for maximum overhead :)
	 */
	GkrellmPiximage   *webkit_bg = NULL;
	WebKitWebSettings *settings;
	GdkPixbuf         *webkit_scaled_bg;
	gchar             *webkit_scaled_bg_xpm;
	gsize              webkit_scaled_bg_xpm_size;
	gchar             *b64_png;
	gchar             *datauri_png;
	gchar             *css_rule;
	gchar             *b64_css_rule;
	GError            *err = NULL;
	gchar             *css;
	webkit_bg = gkrellm_bg_panel_piximage(0);
	webkit_scaled_bg = gkrellm_scale_piximage_to_pixbuf(
		webkit_bg,
		gkrellm_chart_width(),
		gwk_cfg_frames[i].height
	);
	gdk_pixbuf_save_to_buffer(
		webkit_scaled_bg,
		&webkit_scaled_bg_xpm,
		&webkit_scaled_bg_xpm_size,
		"png",
		&err,
		"compression",
		"0",
		NULL
	);
	b64_png = g_base64_encode(
		(guchar *)webkit_scaled_bg_xpm,
		webkit_scaled_bg_xpm_size
	);
	datauri_png = g_strconcat(
		"data:image/png;charset=utf-8;base64,",
		b64_png,
		NULL
	);
	css_rule = g_strconcat(
		"html, body { background: transparent url(",
		datauri_png,
		") no-repeat 0% 0% !important; }",
		NULL
	);
	b64_css_rule = g_base64_encode(
		(guchar *)css_rule,
		strlen((gchar *)css_rule)
	);
	settings = webkit_web_view_get_settings(gwk_cfg_frames[i].web_view);
	css = g_strconcat(
		"data:text/css;charset=utf-8;base64,",
		b64_css_rule,
		NULL
	);
	g_object_set(
		G_OBJECT(settings),
		"user-stylesheet-uri",
		css,
		NULL
	);
	webkit_web_view_set_settings(gwk_cfg_frames[i].web_view, settings);
} /* }}} */
static void gwk_webkit_frame_resize(int i) { /* {{{ */
	g_object_set(
		G_OBJECT(gwk_cfg_frames[i].web_view),
		"height-request",
		gwk_cfg_frames[i].height,
		NULL
	);
	g_object_set(
		G_OBJECT(gwk_cfg_frames[i].web_view),
		"width-request",
		gkrellm_chart_width(),
		NULL
	);
} /* }}} */
static void gwk_create_web_view(int i) { /* {{{ */

	gwk_cfg_frames[i].web_view =
		WEBKIT_WEB_VIEW(webkit_web_view_new());
	gwk_apply_webkit_bg(i);
	webkit_web_view_load_uri(
		WEBKIT_WEB_VIEW(gwk_cfg_frames[i].web_view),
		gwk_cfg_frames[i].uri
	);
	g_signal_connect(
		G_OBJECT(gwk_cfg_frames[i].web_view),
		"create-web-view",
		G_CALLBACK(gwk_create_web_view_cb),
		GINT_TO_POINTER(i)
	);
	g_signal_connect(
		G_OBJECT(gwk_cfg_frames[i].web_view),
		"new-window-policy-decision-requested",
		G_CALLBACK(gwk_new_window_policy_decision_requested_cb),
		GINT_TO_POINTER(i)
	);
	gwk_webkit_frame_resize(i);
	gtk_box_pack_start(
		GTK_BOX(gwk_gk_vbox),
		GTK_WIDGET(gwk_cfg_frames[i].web_view),
		FALSE,
		FALSE,
		0
	);
} /* }}} */
/* }}} */

/* Callbacks {{{ */
static WebKitWebView* gwk_create_web_view_cb( /* {{{ */
	WebKitWebView  *web_view,
	WebKitWebFrame *frame,
	gpointer        user_data
) {
	gchar *cmd;
	int    res;
	/* We do not use web_view nor frame; shut up warning */
	(void)web_view;
	(void)frame;
	cmd = g_strdup_printf(
		"%s '%s' &",
		gwk_cfg_browser,
		gwk_cfg_frames[GPOINTER_TO_INT(user_data)].popup_uri
	);
	if ((res = system(cmd))) {
		g_error(
			"Failed opening %s - result is %d\n",
			gwk_cfg_frames[GPOINTER_TO_INT(user_data)].popup_uri,
			res
		);
	};
	g_free(cmd);
	return NULL;
} /* }}} */
static gboolean gwk_new_window_policy_decision_requested_cb( /* {{{ */
	WebKitWebView             *web_view,
	WebKitWebFrame            *frame,
	WebKitNetworkRequest      *request,
	WebKitWebNavigationAction *navigation_action,
	WebKitWebPolicyDecision   *policy_decision,
	gpointer                   user_data
) {
	/* We do not use all callback variables; shut up warnings */
	(void)web_view;
	(void)frame;
	(void)navigation_action;
	(void)policy_decision;
	/* Set popup URI */
	gwk_cfg_frames[GPOINTER_TO_INT(user_data)].popup_uri =
		g_strdup(webkit_network_request_get_uri(request));
	return FALSE;
} /* }}} */
static void gwk_cfg_browser_cb( /* {{{ */
	GtkWidget *widget,
	gpointer data
) {
	/* We do not use data, shut up warning */
	(void)data;
	gwk_cfg_browser = g_strdup(
		gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(widget))
	);
	gkrellm_config_modified();
} /* }}} */
static void gwk_cfg_height_cb( /* {{{ */
	GtkWidget *widget,
	gpointer data
) {
	gwk_cfg_frames[GPOINTER_TO_INT(data)].height =
		gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widget));
	g_object_set(
		G_OBJECT(gwk_cfg_frames[GPOINTER_TO_INT(data)].web_view),
		"height-request",
		gwk_cfg_frames[GPOINTER_TO_INT(data)].height,
		NULL
	);
	gwk_reallocate_monitor_height();
	gwk_apply_webkit_bg(GPOINTER_TO_INT(data));
	gkrellm_config_modified();
} /* }}} */
static void gwk_cfg_period_cb( /* {{{ */
	GtkWidget *widget,
	gpointer data
) {
	gwk_cfg_frames[GPOINTER_TO_INT(data)].period =
		gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widget));
	gkrellm_config_modified();
} /* }}} */
static void gwk_cfg_uri_cb( /* {{{ */
	GtkWidget *widget,
	gpointer data
) {
	gwk_cfg_frames[GPOINTER_TO_INT(data)].uri =
		g_strdup(gtk_entry_get_text(GTK_ENTRY(widget)));
	webkit_web_view_load_uri(
		WEBKIT_WEB_VIEW(gwk_cfg_frames[GPOINTER_TO_INT(data)].web_view),
		gwk_cfg_frames[GPOINTER_TO_INT(data)].uri
	);
	gkrellm_config_modified();
} /* }}} */
static void gwk_cfg_uri_event_cb( /* {{{ */
	GtkWidget *widget,
	GdkEvent *event,
	gpointer data
) {
	/* We do not use event, shut up warning */
	(void)event;
	gwk_cfg_uri_cb(widget, data);
} /* }}} */
static void gwk_cfg_num_used_frames_cb( /* {{{ */
	GtkWidget *widget,
	gpointer data
) {
	int n;
	/* We do not use data, shut up warning */
	(void)data;
	n = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widget));
	if (n > gwk_cfg_used_frames) {
		do {
			gwk_cfg_used_frames++;
			gwk_create_tab_frame(gwk_cfg_used_frames - 1);
			gwk_create_web_view(gwk_cfg_used_frames - 1);
		} while (gwk_cfg_used_frames != n);
	}
	else if (n < gwk_cfg_used_frames) {
		do {
			gwk_remove_tab_frame(gwk_cfg_used_frames - 1);
			gtk_widget_destroy(
				GTK_WIDGET(
					gwk_cfg_frames[gwk_cfg_used_frames - 1].web_view
				)
			);
			gwk_cfg_used_frames--;
		} while (gwk_cfg_used_frames != n);
	};
	gwk_reallocate_monitor_height();
	/* Refresh web view display */
	gtk_widget_show_all(GTK_WIDGET(gwk_gk_vbox));
	/* Refresh notebook display */
	gtk_widget_show_all(GTK_WIDGET(gwk_tabs));
	gkrellm_config_modified();
} /* }}} */
/* }}} */

/* GKrellM interface implementation {{{ */
static GkrellmMonitor gwk_plugin_mon = { /* {{{ */
	GWK_PLUGIN_NAME,        /* Name, for config tab.                    */
	0,                      /* Id,  0 if a plugin                       */
	gwk_create_plugin,      /* The create_plugin() function             */
	gwk_update_plugin,      /* The update_plugin() function             */
	gwk_create_tab,         /* The create_plugin_tab() config function  */
	NULL,                   /* The apply_plugin_config() function       */
	gwk_save_plugin_config, /* The save_plugin_config() function        */
	gwk_load_plugin_config, /* The load_plugin_config() function        */
	GWK_PLUGIN_KEYWORD,     /* config keyword                           */
	NULL,                   /* Undefined 2                              */
	NULL,                   /* Undefined 1                              */
	NULL,                   /* Undefined 0                              */
	GWK_PLUGIN_PLACEMENT,   /* Insert plugin before this monitor.       */
	NULL,                   /* Handle if a plugin, filled in by GKrellM */
	NULL                    /* path if a plugin, filled in by GKrellM   */
}; /* }}} */
GkrellmMonitor * gkrellm_init_plugin(void) { /* {{{ */
	int i;
	/* Define WebKit frame instances */
	gwk_cfg_frames  = g_new0(gwk_frame, GWK_MAX_FRAMES);
	gwk_cfg_used_frames = 1;
	for (i = 0; i < GWK_MAX_FRAMES; i++) {
		gwk_cfg_frames[i].uri    = "";
		gwk_cfg_frames[i].height = GWK_STD_FRAME_HEIGHT;
		gwk_cfg_frames[i].period = 0;
	};

	gwk_ticks = gkrellm_ticks();

	/* Setup default configuration values */
	gkrellm_dup_string(&gwk_cfg_browser, GWK_DEFAULT_BROWSER);

	/* Setup gettext */
	bindtextdomain(PACKAGE, LOCALEDIR);

	return (gwk_monitor = &gwk_plugin_mon);
} /* }}} */
static void gwk_create_plugin( /* {{{ */
	GtkWidget *vbox,
	gint first_create
) {
	int i;
	gwk_gk_vbox = vbox;
	for (i = 0; i < gwk_cfg_used_frames; i++) {
		if (first_create) {
			gwk_create_web_view(i);
		}
		else {
			gwk_webkit_frame_resize(i);
			gwk_apply_webkit_bg(i);
		}
	};
	gwk_reallocate_monitor_height();
	gtk_widget_show_all(GTK_WIDGET(gwk_gk_vbox));
} /* }}} */
static void gwk_update_plugin() { /* {{{ */
	int i;
	for (i = 0; i < gwk_cfg_used_frames; i++) {
		if (
			(
				gwk_cfg_frames[i].period > 0
			)
			&&
			(
				/* Avoid reload on very first tick */
				(gwk_ticks->timer_ticks + 1)
				%
				(gwk_cfg_frames[i].period * gkrellm_update_HZ())
				==
				0
			)
		) {
			webkit_web_view_reload(gwk_cfg_frames[i].web_view);
		};
	};
} /* }}} */
static void gwk_create_tab(GtkWidget *tab_vbox) { /* {{{ */
	int i;
	if (gwk_tabs) {
		g_object_unref(G_OBJECT(gwk_tabs));
	};
	gwk_tabs = gtk_notebook_new();
	gtk_notebook_set_tab_pos(GTK_NOTEBOOK(gwk_tabs), GTK_POS_TOP);
	gtk_box_pack_start(
		GTK_BOX(tab_vbox),
		GTK_WIDGET(gwk_tabs),
		TRUE,
		TRUE,
		0
	);
	g_object_ref(G_OBJECT(gwk_tabs));
	gwk_create_tab_setup();
	for (i = 0; i < gwk_cfg_used_frames; i++) {
		gwk_create_tab_frame(i);
	};
	gwk_create_tab_info();
	gwk_create_tab_about();
} /* }}} */
static void gwk_load_plugin_config(gchar *arg) { /* {{{ */
	gchar config[CFG_BUFSIZE];
	gchar value[CFG_BUFSIZE];
	gchar frame_key[CFG_BUFSIZE];
	gchar frame_value[CFG_BUFSIZE];
	gint  n;
	gint  frame_number;

	n = sscanf(arg, "%s %[^\n]", config, value);
	if (n != 2) {
		return;
	};
	if (!strcmp(config, "browser")) {
		gwk_cfg_browser = g_strdup(value);
	}
	else if (!strcmp(config, "frames")) {
		gwk_cfg_used_frames = atoi(value);
	}
	else if (!strcmp(config, "frame")) {
		n = sscanf(
			value,
			"%d %s %s",
			&frame_number,
			frame_key,
			frame_value
		);
		if (n != 3) {
			return;
		};
		if (!strcmp(frame_key, "uri")) {
			gwk_cfg_frames[frame_number].uri = g_strdup(frame_value);
		}
		else if (!strcmp(frame_key, "height")) {
			gwk_cfg_frames[frame_number].height = atoi(frame_value);
		}
		else if (!strcmp(frame_key, "period")) {
			gwk_cfg_frames[frame_number].period = atoi(frame_value);
		};
	};
} /* }}} */
static void gwk_save_plugin_config(FILE *f) { /* {{{ */
	int i;
	fprintf(
		f,
		"%s browser %s\n",
		GWK_PLUGIN_KEYWORD,
		gwk_cfg_browser
	);
	fprintf(
		f,
		"%s frames %d\n",
		GWK_PLUGIN_KEYWORD,
		gwk_cfg_used_frames
	);
	for (i = 0; i < GWK_MAX_FRAMES; i++) {
		fprintf(
			f,
			"%s frame %d uri %s\n",
			GWK_PLUGIN_KEYWORD,
			i,
			gwk_cfg_frames[i].uri
		);
		fprintf(
			f,
			"%s frame %d height %d\n",
			GWK_PLUGIN_KEYWORD,
			i,
			gwk_cfg_frames[i].height
		);
		fprintf(
			f,
			"%s frame %d period %d\n",
			GWK_PLUGIN_KEYWORD,
			i,
			gwk_cfg_frames[i].period
		);
	};
} /* }}} */
/* }}} */

